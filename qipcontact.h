/*****************************************************************************
* qipcontact.h
*
* Created: 12 October 2014 by zettdaymond
*
* Copyright 2014 zettdaymond (zettday@gmail.com). All rights reserved.
*
* This file may be distributed under the terms of GNU Public License version
* 3 (GPL v3) as defined by the Free Software Foundation (FSF). A copy of the
* license should have been included with this file, or the project in which
* this file belongs to. You may also find the details of GPL v3 at:
* http://www.gnu.org/licenses/gpl-3.0.txt
*
* If you have any questions regarding the use of this file, feel free to
* contact the author of this file, or the owner of the project in which
* this file belongs to.
*****************************************************************************/
#ifndef QIPCONTACT_H
#define QIPCONTACT_H

#include <QObject>
#include <QVector>
#include <QString>
#include "message.h"

class QIPContact : public QObject
{
    Q_OBJECT
public:
    explicit QIPContact(QObject *parent = 0);
    QIPContact &operator =(const QIPContact& c);
    QIPContact(const QIPContact &c);
    QIPContact(QIPContact &c);

   QString GetFormatedMessages(QString my_nick);
   QString GetHtmlMessages(QString my_nick);
   void PushMessage(Message message);

   QString UID() const;
   void setUID(const QString &UID);

   QString nick() const;
   void setNick(const QString &nick);

   QVector<Message> messages() const;

signals:

public slots:

private:
   QVector<Message> _messages;
   QString _UID;
   QString _nick;
};


#endif // QIPCONTACT_H

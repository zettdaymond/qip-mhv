/*****************************************************************************
* message.cpp
*
* Created: 12 October 2014 by zettdaymond
*
* Copyright 2014 zettdaymond (zettday@gmail.com). All rights reserved.
*
* This file may be distributed under the terms of GNU Public License version
* 3 (GPL v3) as defined by the Free Software Foundation (FSF). A copy of the
* license should have been included with this file, or the project in which
* this file belongs to. You may also find the details of GPL v3 at:
* http://www.gnu.org/licenses/gpl-3.0.txt
*
* If you have any questions regarding the use of this file, feel free to
* contact the author of this file, or the owner of the project in which
* this file belongs to.
*****************************************************************************/
#include "message.h"

Message::Message(QObject *parent) :
    QObject(parent)
{
}

Message &Message::operator =(const Message &c)
{
    this->_datetime = c.getDatetime();
    this->_text = c.getText();
    this->_IsMyMessage = c.IsMyMessage();
    return *this;
}

Message::Message(const Message &m) :
    QObject()
{
    this->_datetime = m.getDatetime();
    this->_IsMyMessage = m.IsMyMessage();
    this->_text = m.getText();
}


Message::Message(Message &m) :
    QObject()
{
    this->_datetime = m.getDatetime();
    this->_IsMyMessage = m.IsMyMessage();
    this->_text = m.getText();
}

QDateTime Message::getDatetime() const
{
    return _datetime;
}

void Message::setDatetime(const QDateTime &datetime)
{
    _datetime = datetime;
}

void Message::setUnixTime(const unsigned long unixtime)
{
    quint64 time = unixtime;
    time*=1000;
    _datetime = QDateTime::fromMSecsSinceEpoch(time);
}
QString Message::getText() const
{
    return _text;
}

void Message::setText(const QString &text)
{
    _text = text;
}

bool Message::IsMyMessage() const
{
    return this->_IsMyMessage;
}

void Message::setOwnerMessage(const bool _isMyMessage)
{
    _IsMyMessage = _isMyMessage;
}

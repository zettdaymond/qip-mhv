# QIP MHV #

QIP MHV is a simple QIP Mobile History Viewer that provides export in TXT file format. 

## Version ##
* current version is **0.1 (not stable)**

## Features  ##
* opens QIP Mobile/QIP Infium(not tested) *.qhf binary file format.
* user-friendly output to the screen
* export contact history to TXT
* cross-platform (uses Qt)

## Dependencies ##
QIP MHV requires Qt 5.x on your machine to build or/and run. Not tested with older versions of Qt.
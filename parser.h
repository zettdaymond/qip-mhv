/*****************************************************************************
* parser.h
*
* Created: 12 October 2014 by zettdaymond
*
* Copyright 2014 zettdaymond (zettday@gmail.com). All rights reserved.
*
* This file may be distributed under the terms of GNU Public License version
* 3 (GPL v3) as defined by the Free Software Foundation (FSF). A copy of the
* license should have been included with this file, or the project in which
* this file belongs to. You may also find the details of GPL v3 at:
* http://www.gnu.org/licenses/gpl-3.0.txt
*
* If you have any questions regarding the use of this file, feel free to
* contact the author of this file, or the owner of the project in which
* this file belongs to.
*****************************************************************************/
#ifndef PARSER_H
#define PARSER_H

#include <QObject>
#include <QFile>
#include "qipcontact.h"

class Parser : public QObject
{
    Q_OBJECT
public:
    explicit Parser(QObject *parent = 0);
    QIPContact Parse(QFile &file);
private :
    unsigned long ParseNumber(QFile &file, short size);
    Message ParseMessage(QFile &file);
signals:

public slots:

private:

};

#endif // PARSER_H

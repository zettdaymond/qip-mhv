/*****************************************************************************
* message.h
*
* Created: 12 October 2014 by zettdaymond
*
* Copyright 2014 zettdaymond (zettday@gmail.com). All rights reserved.
*
* This file may be distributed under the terms of GNU Public License version
* 3 (GPL v3) as defined by the Free Software Foundation (FSF). A copy of the
* license should have been included with this file, or the project in which
* this file belongs to. You may also find the details of GPL v3 at:
* http://www.gnu.org/licenses/gpl-3.0.txt
*
* If you have any questions regarding the use of this file, feel free to
* contact the author of this file, or the owner of the project in which
* this file belongs to.
*****************************************************************************/
#ifndef MESSAGE_H
#define MESSAGE_H

#include <QObject>
#include <QString>
#include <QDateTime>
class Message : public QObject
{
    Q_OBJECT
public:
    explicit Message(QObject *parent = 0);
    Message &operator =(const Message& c);
    Message(const Message &m);
    Message(Message &m);

    QDateTime getDatetime() const;
    void setDatetime(const QDateTime &getDatetime);
    void setUnixTime(const unsigned long unixtime);
    QString getText() const;
    void setText(const QString &getText);

    bool IsMyMessage() const;
    void setOwnerMessage(const bool _isMyMessage);
signals:

public slots:

private:
    bool _IsMyMessage;
    QDateTime _datetime;
    QString _text;
};

#endif // MESSAGE_H

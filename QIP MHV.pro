QT       += core gui
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QIP_MHV
TEMPLATE = app

QMAKE_TARGET_PRODUCT = QIP_MHV
QMAKE_TARGET_DESCRIPTION = Simple QIP Mobile History Viever that provides export in TXT file format.
QMAKE_TARGET_COPYRIGHT = zettday@gmail.com

VERSION = 0.1
Release:DEFINES += QT_NO_DEBUG_OUTPUT

DEFINES += APP_VERSION=\\\"$$VERSION\\\" TARGET_PRODUCT=\\\"$$QMAKE_TARGET_PRODUCT\\\"

SOURCES += main.cpp\
        mainwindow.cpp \
    parser.cpp \
    qipcontact.cpp \
    message.cpp

HEADERS  += mainwindow.h \
    parser.h \
    qipcontact.h \
    message.h

FORMS    += mainwindow.ui

RESOURCES += \
    resource.qrc

/*****************************************************************************
* mainwindow.cpp
*
* Created: 12 October 2014 by zettdaymond
*
* Copyright 2014 zettdaymond (zettday@gmail.com). All rights reserved.
*
* This file may be distributed under the terms of GNU Public License version
* 3 (GPL v3) as defined by the Free Software Foundation (FSF). A copy of the
* license should have been included with this file, or the project in which
* this file belongs to. You may also find the details of GPL v3 at:
* http://www.gnu.org/licenses/gpl-3.0.txt
*
* If you have any questions regarding the use of this file, feel free to
* contact the author of this file, or the owner of the project in which
* this file belongs to.
*****************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "parser.h"
#include <QHBoxLayout>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //provide auto-stretch when form resized.
    ui->centralWidget->setLayout(ui->horizontalLayout);

    //menu actions and handlers
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(OpenFile()));
    connect(ui->actionOpen_Folder, SIGNAL(triggered()),this,SLOT(OpenFolder()));

    //provide navigation on contact list
    connect(ui->listWidget, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)),
            this, SLOT(updateTextBox(QListWidgetItem*,QListWidgetItem*)));
    connect(ui->actionTxt, SIGNAL(triggered()), this, SLOT(serializeContactAsTxt()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::OpenFile()
{
    QString fileName = QFileDialog::getOpenFileName(this);

    ui->textEdit->clear();
    ui->listWidget->clear();
    _contactHashTable.clear();
    if (!fileName.isEmpty())
        LoadFile(fileName);


}

void MainWindow::OpenFolder()
{
    ui->textEdit->clear();
    ui->listWidget->clear();
    _contactHashTable.clear();

    QFileDialog dialog;
    dialog.setFileMode(QFileDialog::Directory);
    dialog.setOption(QFileDialog::ShowDirsOnly);

    QString directoryName = dialog.getExistingDirectory(this);

    if(!directoryName.isEmpty()) {
        loadDirectory(directoryName);
    }

}

void MainWindow::LoadFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
    {
       QMessageBox::warning(this,
                            tr("Error"),
                            tr("Could not open file: %1, \n %2")
                            .arg(fileName)
                            .arg(file.errorString()));
    }

    //file processing and obtaining contact history
    Parser parser;
    QIPContact con = parser.Parse(file);
    _contactHashTable[con.UID()] = con;

    //adding in contact list
    ui->listWidget->addItem(con.UID());
    file.close();
}

void MainWindow::loadDirectory(const QString &dirName)
{
    QDir dir(dirName+"/");
    dir.setFilter(QDir::Files);
    dir.setNameFilters(QStringList() << "*.qhf");
    QFileInfoList list = dir.entryInfoList();
    for (QFileInfo file : list) {
        if (file.exists()) {
            LoadFile(file.filePath());
        }
    }
}

void MainWindow::updateTextBox(QListWidgetItem *first, QListWidgetItem*)
{
    if (first!=nullptr && _contactHashTable.contains(first->text()) ) {
        QIPContact con =  _contactHashTable[ first->text() ];
        ui->textEdit->setHtml(con.GetHtmlMessages(MY_NICK));
    }
}

void MainWindow::serializeContactAsTxt()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);

    QString fileName = dialog.getSaveFileName(this);

    if (fileName.isEmpty() == false) {
        saveTxtFile(fileName);
    }
}

void MainWindow::saveTxtFile(QString fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
       QMessageBox::warning(this,
                            tr("Error"),
                            tr("Could not open file: %1, \n %2")
                            .arg(fileName)
                            .arg(file.errorString()));
       return;
    }

    //file processing and obtaining contact history
    QTextStream stream(&file);
    if (ui->listWidget->currentItem() != nullptr &&
       _contactHashTable.contains(ui->listWidget->currentItem()->text()))
    {
        QIPContact contact = _contactHashTable[ui->listWidget->currentItem()->text()];
        stream << "CONTACT UID : " + contact.UID() << "\n"
               << "MY NICK :" << MY_NICK << "\n";
        stream << contact.GetFormatedMessages(MY_NICK);
    }
    file.close();
}

void MainWindow::pushNewContact(const QIPContact contact)
{
    _contactList.push_back(contact);
}




/*****************************************************************************
* mainwindow.h
*
* Created: 12 October 2014 by zettdaymond
*
* Copyright 2014 zettdaymond (zettday@gmail.com). All rights reserved.
*
* This file may be distributed under the terms of GNU Public License version
* 3 (GPL v3) as defined by the Free Software Foundation (FSF). A copy of the
* license should have been included with this file, or the project in which
* this file belongs to. You may also find the details of GPL v3 at:
* http://www.gnu.org/licenses/gpl-3.0.txt
*
* If you have any questions regarding the use of this file, feel free to
* contact the author of this file, or the owner of the project in which
* this file belongs to.
*****************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QDataStream>
#include <QHash>
#include <QDir>
#include <QListWidgetItem>
#include <QTextStream>
#include "qipcontact.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void OpenFile();
    void OpenFolder();
    void LoadFile(const QString &fileName);
    void loadDirectory(const QString &dirName);
    void updateTextBox(QListWidgetItem* first, QListWidgetItem *);
    void serializeContactAsTxt();
    void saveTxtFile(QString fileName);


private :
    void pushNewContact(const QIPContact contact);
    const QString MY_NICK = "I";
private:
    Ui::MainWindow *ui;
    QVector<QIPContact> _contactList;
    QHash<QString, QIPContact> _contactHashTable;
};

#endif // MAINWINDOW_H

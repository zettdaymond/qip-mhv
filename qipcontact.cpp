/*****************************************************************************
* qipcontact.cpp
*
* Created: 12 October 2014 by zettdaymond
*
* Copyright 2014 zettdaymond (zettday@gmail.com). All rights reserved.
*
* This file may be distributed under the terms of GNU Public License version
* 3 (GPL v3) as defined by the Free Software Foundation (FSF). A copy of the
* license should have been included with this file, or the project in which
* this file belongs to. You may also find the details of GPL v3 at:
* http://www.gnu.org/licenses/gpl-3.0.txt
*
* If you have any questions regarding the use of this file, feel free to
* contact the author of this file, or the owner of the project in which
* this file belongs to.
*****************************************************************************/
#include "qipcontact.h"

QIPContact::QIPContact(QObject *parent) :
    QObject(parent)
{
}

QIPContact& QIPContact::operator =(const QIPContact &c)
{
    _UID = c.UID();
    _nick = c.nick();
    _messages = c.messages();
    return *this;

}


QIPContact::QIPContact(const QIPContact &c) :
    QObject()
{
    _UID = c.UID();
    _nick = c.nick();
    _messages = c.messages();
}

QIPContact::QIPContact(QIPContact &c):
    QObject()
{

    _UID = c.UID();
    _nick = c.nick();
    _messages = c.messages();
}



QString QIPContact::GetFormatedMessages(QString my_nick = "I")
{
   QString exp = "";

   for ( Message mes : _messages )
   {
       exp += "\n";
       if ( mes.IsMyMessage() == true )
       {
           exp += my_nick;
       }
       else
       {
           exp += _nick;
       }
       exp +=  " (" + mes.getDatetime().toString(Qt::SystemLocaleShortDate) + ") ";
       exp += "\n";
       exp += mes.getText();
       exp += "\n";
   }
   return exp;
}

QString QIPContact::GetHtmlMessages(QString my_nick = "I")
{
    QString exp = "";

    QString my_name = "<font color=\"Blue\">";
    QString contact_nick = "<font color=\"Red\">";
    QString date = "<font color=\"Green\">";
    QString endfont = "</font>";
    QString endHtml = "</br>";
    QString startHtml = "<br>";

    for ( Message mes : _messages )
    {

        exp += startHtml;
        if ( mes.IsMyMessage() == true )
        {
            exp += my_name + my_nick + endfont;
        }
        else
        {
            exp += contact_nick + _nick + endfont;
        }
        exp += date + " (" + mes.getDatetime().toString(Qt::SystemLocaleShortDate) + ")"+ endfont + endHtml +"\n";
        exp += startHtml;
        exp += mes.getText()+endHtml;
        exp += startHtml + "\n" + endHtml;
    }
    return exp;
}

void QIPContact::PushMessage(Message message)
{
    _messages.push_back(message);
}
QString QIPContact::UID() const
{
    return _UID;
}

void QIPContact::setUID(const QString &UID)
{
    _UID = UID;
}
QString QIPContact::nick() const
{
    return _nick;
}

void QIPContact::setNick(const QString &nick)
{
    _nick = nick;
}
QVector<Message> QIPContact::messages() const
{
    return _messages;
}

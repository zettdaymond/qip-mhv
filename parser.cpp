/*****************************************************************************
* parser.cpp
*
* Created: 12 October 2014 by zettdaymond
*
* Copyright 2014 zettdaymond (zettday@gmail.com). All rights reserved.
*
* This file may be distributed under the terms of GNU Public License version
* 3 (GPL v3) as defined by the Free Software Foundation (FSF). A copy of the
* license should have been included with this file, or the project in which
* this file belongs to. You may also find the details of GPL v3 at:
* http://www.gnu.org/licenses/gpl-3.0.txt
*
* If you have any questions regarding the use of this file, feel free to
* contact the author of this file, or the owner of the project in which
* this file belongs to.
*****************************************************************************/
#include "parser.h"
#include <QtEndian>
#include <QDataStream>
Parser::Parser(QObject *parent) :
    QObject(parent)
{

}

QIPContact Parser::Parse(QFile &file)
{
    //parsing file header
    file.seek(34);
    unsigned long messageCount = ParseNumber(file, 4);

    file.seek(44);
    unsigned long UIN_Lenght = ParseNumber(file, 2);

    QString UIN (file.read(UIN_Lenght));

    unsigned long NICK_Lenght = ParseNumber(file, 2);

    QString NICK (file.read(NICK_Lenght));

    QIPContact new_contact;
    new_contact.setNick(NICK);
    new_contact.setUID(UIN);

    //parsing contact messages
    for (unsigned long i = 0; i < messageCount; i++) {
        new_contact.PushMessage( ParseMessage(file) );
    }
    return new_contact;
}

unsigned long Parser::ParseNumber(QFile &file, short size)
{
    QByteArray arr = file.read(size);
    QDataStream stream(&arr,QIODevice::ReadOnly);
    stream.setByteOrder(QDataStream::BigEndian);
    long res;
    switch (size) {
    case 2:
        qint16 tmp16;
        stream >> tmp16;
        res = tmp16;
        break;
    case 4:
        qint32 tmp32;
        stream >> tmp32;
        res = tmp32;
        break;
    case 1:
        qint8 tmp8;
        stream >> tmp8;
        res = tmp8;
        break;
    default :
        throw;
        break;
    }
    return  res;
}

Message Parser::ParseMessage(QFile &file)
{
    Message outputMsg;
    unsigned long messageLenght = 0;

    file.seek(18 + file.pos());
    unsigned long time = ParseNumber(file,4);

    file.seek(4 + file.pos());
    bool isMyMsg = ParseNumber(file, 1);

    file.seek(4 + file.pos());
    messageLenght = ParseNumber(file, 2);

    QString mes( file.read(messageLenght) );

    outputMsg.setUnixTime(time);
    outputMsg.setText(mes);
    outputMsg.setOwnerMessage(isMyMsg);

    return outputMsg;
}

